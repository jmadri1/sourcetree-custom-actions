#!/bin/bash

#Get the arguments passed into Sourcetree
args=("$@")
argsCount=("$#")

branch=$(git symbolic-ref --short HEAD)

echo "Commit SHA1: $1"
echo "Branch name: $branch"

function create_dir() {
  mkdir $branch
  echo "created"
}

function export_changes() {

  # echo " # # # # #  Bash:   S c r i p t   I n i t i a t e d   # # # # # " 
  # echo  " - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - " 

  local giturl=$(git config --get remote.origin.url)

  if [ "$giturl" == "" ]
    then
      echo "Not a git repository or no remote.origin.url set"
      exit 1;
    else
      if (("$argsCount"  > "2"))
        then
          echo " # # # # #  Bash:  Please select only two commit using cmd click instead of shift.  # # # # # " 
          echo  " - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "  1>&2
          exit 64;
      fi

      if ((-d ${branch}))
        then
          echo "exist"
        else
          echo $(create_dir)
      fi

      git archive -o $branch/$branch.zip HEAD $(git diff-tree -r --no-commit-id --name-only --diff-filter=ACMRT ${args[1]} ${args[0]})
      exit 1;
  fi
}

echo $(export_changes)
